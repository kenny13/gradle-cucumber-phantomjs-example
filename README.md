gradle-cucumber-phantomjs-example
=================================

This sample project demonstrates use of following technologies :
 * Cucumber 1.1.3 (newest)
 * Gradle tested on 1.6
 * GhostDrive 1.0.3
 * PhantomJs 1.9.1 (executable for windows only)
 * Selenium 2.33.0

Basically with this teh.stack you can run Selenium Web UI Integration tests on every platform using WebKit headless browser !

**! Take note due to bug in gradle-cucumber junit tests result parsing ( [GRADLE-2615](http://issues.gradle.org/browse/GRADLE-2615) -  still not fixed) you will need patch gradle your self [patch example](https://github.com/kenny13/gradle/commit/8653bcee24413e50196826ec9a250589fd05c30c) .**

### Test scenario:

<pre>
Feature: Search on Google
  As an user
  I want to search on Google
  So that I can see results

  Scenario: results are shown
    Given the page is open "http://www.google.com"
    When I search for "Cucumber"
    Then a browser title should contains "Google"
</pre>
